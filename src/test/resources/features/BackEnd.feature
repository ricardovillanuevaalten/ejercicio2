# new feature
# Tags: optional

Feature: Feature to learn how to test the BackEnd

  Scenario Outline: Get an existing pet in the store
    When I request to get a user by id "<userID>"
    Then I should get expected code "<expectedCode>"
    And  The value for "<parameter>" after the get operation should be "<parameterValue>"
    Examples:
      | userID              | expectedCode | parameter | parameterValue |
      | 3456789876544335670 | 200          | name      | candy          |

  Scenario Outline: Post a pet in the store and then check that is created gettintg it
    When I post a new pet with id "<userID>" and "<parameter>" "<parameterValue>"
    Then I should get expected code "<expectedCode>"
    When I request to get a user by id "<userID>"
    Then I should get expected code "<expectedCode>"
    And  The value for "<parameter>" after the get operation should be "<parameterValue>"
    When I request to put a user by id "<userID>"
    Then I should get expected code "<expectedCode>"
    And  The value for "<parameter2>" after the put operation should be "<parameterValue2>"
    When I request to delete a user by id "<userID>"
    Then I should get expected code "<expectedCode>"
    When I request to get a user by id "<userID>"
    Then I should get expected code "<expectedCode2>"
    Examples:
      | userID   | expectedCode | expectedCode2| parameter | parameter2 | parameterValue | parameterValue2 |
      | 55555555 | 200          | 404          | name      | status     | pepe           | sold            |