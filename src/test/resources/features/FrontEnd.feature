# new feature
# Tags: optional

  @ready
  Feature: Search travellings with different options

  @test
  Scenario Outline: Search travelling
    Given I want to search for trains
    When I select origin from "<originStation>" y destination from "<destinationStation>"
    And I introduce the departure date '1' day after current day
    And I introduce the return date '2' days after the departure date
    And I select the next adults and children
      | adults  | children  | babies |
      | 3       | 2         | 1      |
    And I click on buy
    Then I should see the results page

    Examples:
      | originStation            | destinationStation |
      | VALLADOLID               | MADRID-CHAMARTIN   |
      | MADRID-PUERTA DE ATOCHA  | BARCELONA-SANTS    |