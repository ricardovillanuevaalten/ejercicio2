package com.ejercicios.act.features.stepDefinitions;

import com.ejercicios.act.features.serenitySteps.RenfeSearchSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Given;
import cucumber.api.DataTable;
import net.thucydides.core.annotations.Steps;
import sun.security.util.PendingException;
import com.ejercicios.act.features.testRunner.RenfeSearchTest;

import java.util.List;
import java.util.Map;

public class RenfeStepDefinitions {

    @Steps private RenfeSearchSteps renfeSearchSteps;

    @Given("I want to search for trains")
    public void iWantToSearchForTrains(){
        renfeSearchSteps.openRenfeSearchPage();
    }

    @When("I select origin from \"([^\"]*)\" y destination from \"([^\"]*)\"")
    public void iSelectOriginAndDestination(String originStation, String destinationsStation) throws Throwable{
        renfeSearchSteps.introduceOriginAndDestination(originStation, destinationsStation);
    }

    @And("I introduce the departure date '(.*)' day after current day")
    public void iIntroduceTheDepartureDate(int daysAfterToday){
        renfeSearchSteps.introduceDepartureDate(daysAfterToday);
    }

    @And("I introduce the return date '(.*)' days after the departure date")
    public void iIntroduceTheReturnDate(int daysAfterDeparture){
        renfeSearchSteps.introduceTheReturnDate(daysAfterDeparture);
    }

    @And("I select the next adults and children")
    public void iSelectAdultsAndChildren(DataTable adultsAndChildren){
        List<Map<String, String>> list = adultsAndChildren.asMaps(String.class, String.class);
        renfeSearchSteps.selectAdultsAndChildren(list);
    }

    @And("I click on buy")
    public void iClickOnBuy(){
        renfeSearchSteps.clickOnBuy();
    }

    @Then("I should see the results page")
    public void iShouldSeeTheResultsPage(){
        renfeSearchSteps.verifyResults();
    }
}
