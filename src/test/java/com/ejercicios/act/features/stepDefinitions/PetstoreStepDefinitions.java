package com.ejercicios.act.features.stepDefinitions;

import com.ejercicios.act.features.serenitySteps.PetstoreSearchSteps;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class PetstoreStepDefinitions {

    @Steps private PetstoreSearchSteps petstoreSearchSteps;

    @When("I request to get a user by id \"([^\"]*)\"")
    public void iRequestToGetAUserById(long id) throws Throwable{
        petstoreSearchSteps.requestToGetAUserById(id);
    }

    @Then("I should get expected code \"([^\"]*)\"")
    public void iShouldGetExpectedCode(int code){
        petstoreSearchSteps.getExpectedCode(code);
    }

    @And("The value for \"([^\"]*)\" after the get operation should be \"([^\"]*)\"")
    public void checkValue(String field, String value){
        petstoreSearchSteps.checkValue(field, value);
    }

    @When("^I post a new pet with id \"([^\"]*)\" and \"([^\"]*)\" \"([^\"]*)\"$")
    public void iPostANewPetWithIdAnd(long id, String field, String value) {
        petstoreSearchSteps.postANewPet(id, field, value);
    }

    @When("^I request to put a user by id \"([^\"]*)\"$")
    public void iRequestToPutAUserById(long id) {
        petstoreSearchSteps.requestToPutAUserById(id);
    }

    @And("^The value for \"([^\"]*)\" after the put operation should be \"([^\"]*)\"$")
    public void theValueForAfterThePutOperationShouldBe(String field, String value) {
        petstoreSearchSteps.checkValue(field, value);
    }

    @When("^I request to delete a user by id \"([^\"]*)\"$")
    public void iRequestToDeleteAUserById(long id) {
        petstoreSearchSteps.deleteAPetById(id);
    }
}
