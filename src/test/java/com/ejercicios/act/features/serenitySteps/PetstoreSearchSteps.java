package com.ejercicios.act.features.serenitySteps;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;

import java.io.FileReader;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class PetstoreSearchSteps {

    private Response response;

    @Step
    public void requestToGetAUserById(long id){
        response = given().when().get("https://petstore.swagger.io/v2/pet/"+id);
    }

    @Step
    public void getExpectedCode(int code){
        response.then().assertThat().statusCode(code);
    }

    @Step
    public void checkValue(String field, String value){
        response.then().assertThat().body(field, equalTo(value));
    }

    @Step
    public void postANewPet(long id, String field, String value) {
        String xml = getJsonFromFile("src/test/resources/pets/postPepe.xml");
        response = given().contentType(ContentType.XML).body(xml).post("https://petstore.swagger.io/v2/pet/");
    }

    @Step
    private String getJsonFromFile(String file) {
        String xml = "";
        try(FileReader reader = new FileReader(file)){
            int i;
            while((i =  reader.read())!=-1){
                char ch = (char)i;

                xml += ch;
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return xml;
    }

    @Step
    public void requestToPutAUserById(long id) {
        String xml = getJsonFromFile("src/test/resources/pets/putPepe.xml");
        response = given().contentType(ContentType.XML).body(xml).put("https://petstore.swagger.io/v2/pet/");
    }

    @Step
    public void deleteAPetById(long id) {
        response = given().delete("https://petstore.swagger.io/v2/pet/"+id);
    }
}
