package com.ejercicios.act.features.serenitySteps;

import com.ejercicios.act.features.RenfeResultsPage;
import com.ejercicios.act.features.RenfeSearchPage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class RenfeSearchSteps {

    private RenfeSearchPage searchPage;
    private RenfeResultsPage resultsPage;

    @Step
    public void openRenfeSearchPage(){
        searchPage.open();
        searchPage.getDriver().manage().window().maximize();
    }

    @Step
    public void introduceOriginAndDestination(String originStation, String destinationsStation){
        searchPage.introduceOriginAndDestination(originStation, destinationsStation);
    }

    @Step
    public void introduceDepartureDate(int daysAfterToday){
        searchPage.introduceDepartureDate(daysAfterToday);
    }

    @Step
    public void introduceTheReturnDate(int daysAfterDeparture){
        searchPage.introduceTheReturnDate(daysAfterDeparture);
    }

    @Step
    public void selectAdultsAndChildren(List<Map<String, String>> list){
        searchPage.selectAdultsAndChildren(list);
    }

    @Step
    public void clickOnBuy(){
        searchPage.clickOnBuy();
    }

    @Step
    public void verifyResults(){
        String results = resultsPage.verifyResults();
        Assert.assertTrue(results.contains("https://venta.renfe.com/vol/search"));
    }
}
