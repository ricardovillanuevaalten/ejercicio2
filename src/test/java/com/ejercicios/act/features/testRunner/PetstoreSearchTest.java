package com.ejercicios.act.features.testRunner;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)

@CucumberOptions(
        features = { "src/test/resources/features/BackEnd.feature" },
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber-report.json"},
        glue = { "com.ejercicios.act.features.stepDefinitions" }
)

public class PetstoreSearchTest {
}
