package com.ejercicios.act.features;

import net.serenitybdd.core.pages.PageObject;

public class RenfeResultsPage extends PageObject {
    public String verifyResults() {
        //NO USAR WAITS NI SLEEPS
        return getDriver().getCurrentUrl();
    }
}
