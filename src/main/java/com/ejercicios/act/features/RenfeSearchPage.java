package com.ejercicios.act.features;

import com.opera.core.systems.OperaDriver;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static javax.swing.text.html.CSS.getAttribute;

@DefaultUrl("http://www.renfe.com/")

public class RenfeSearchPage extends PageObject{

    @FindBy (id = "IdOrigen")
    private WebElement originStationInputField;
    @FindBy (id= "ui-id-1")
    private WebElement originStationElementList;
    @FindBy (id = "IdDestino")
    private WebElement destinationStationInputField;
    @FindBy (id= "ui-id-2")
    private WebElement destinationStationElementList;

    @FindBy (id = "__fechaIdaVisual")
    private WebElement departureDateInputField;
    @FindBy (id = "__fechaVueltaVisual")
    private WebElement returnDateInputField;

    @FindBy (id = "__numAdultos")
    private WebElement numAdultsInputField;
    @FindBy (id = "__numNinos")
    private WebElement numChildrenInputField;
    @FindBy (id = "__numBebe")
    private WebElement numBabiesInputField;

    @FindBy (xpath = "//*[@id=\"datosBusqueda\"]/button")
    private WebElement submitButton;

    private int daysAfterToday;

    public RenfeSearchPage(WebDriver driver){
        super(driver);
    }

    public void introduceOriginAndDestination(String originStation, String destinationsStation) {
        element(originStationInputField).type(originStation);
        selectStation(originStationElementList, originStation).click();
        element(destinationStationInputField).type(destinationsStation);
        selectStation(destinationStationElementList, destinationsStation).click();;
    }

    public void introduceDepartureDate(int daysAfterToday) {
        this.daysAfterToday = daysAfterToday;
        element(departureDateInputField).type(
                String.format("%02d", LocalDateTime.now().plusDays(daysAfterToday).getDayOfMonth())+"/"+
                String.format("%02d", LocalDateTime.now().plusDays(daysAfterToday).getMonthValue())+"/"+
                LocalDateTime.now().plusDays(1).getYear()
        );
    }

    public void introduceTheReturnDate(int daysAfterDeparture) {
        element(returnDateInputField).type(
                String.format("%02d", LocalDateTime.now().plusDays(daysAfterToday+daysAfterDeparture).getDayOfMonth())+"/"+
                String.format("%02d", LocalDateTime.now().plusDays(daysAfterToday+daysAfterDeparture).getMonthValue())+"/"+
                LocalDateTime.now().plusDays(1).getYear()
        );
    }

    public void selectAdultsAndChildren(List<Map<String, String>> list) {
        element(numAdultsInputField).type(list.get(0).get("adults"));
        element(numChildrenInputField).type(list.get(0).get("children"));
        element(numBabiesInputField).type(list.get(0).get("babies"));
    }

    public void clickOnBuy() {
        element(submitButton).click();
    }

    private WebElement selectStation(WebElement elements, String originStation) {
        List<WebElement> stations = elements.findElements(By.tagName("li"));
        for (int i = 0; i < stations.size(); i++) {
            if (stations.get(i).getText().equals(originStation)) {
                return stations.get(i);
            }
        }
        return null;
    }
}
